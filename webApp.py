import socket

class WebApp:
    def __init__(self, hostname="", port=1234):
        self.hostname = hostname
        self.port = port
        self.resources = {
            "/": "<html><body><h1>Página principal</h1></body></html>",
            "/hola": "<html><body><h1>Hola mundo</h1></body></html>",
            "/adios": "<html><body><h1>“Adiós mundo cruel</h1></body></html>"
        }
        
    def start_server(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((self.hostname, self.port))
        sock.listen(5)
        print(f"Servidor escuchando en el puerto {self.port}...")

        while True:
            rec_socket, address = sock.accept()
            print(f"Conectado: {address}")
            received = rec_socket.recv(2048).decode('utf-8')

            if not received:
                rec_socket.close()
                continue

            # Obtener la primera línea de la petición HTTP
            request_line = received.split("\n")[0]
            print(f"Petición: {request_line}")

            # Obtener el recurso solicitado
            parts = request_line.split()
            if len(parts) > 1:
                path = parts[1]
            else:
                path = "/"

            # Responder con el contenido adecuado
            if path in self.resources:
                print("Path encontrado")
                content = self.resources[path]
                response = (
                    "HTTP/1.1 200 OK\r\n"
                    "Content-Type: text/html; charset=utf-8\r\n"
                    "Content-Length: " + str(len(content)) + "\r\n"
                    "\r\n" + content
                )
            else:
                content = "<html><body><h1>404 Not Found</h1></body></html>"
                response = (
                    "HTTP/1.1 404 NOT FOUND\r\n"
                    "Content-Type: text/html; charset=utf-8\r\n"
                    "Content-Length: " + str(len(content)) + "\r\n"
                    "\r\n" + content
                )

            rec_socket.send(response.encode('utf-8'))
            rec_socket.close()

# Iniciar el servidor
app = WebApp(port=1234)
app.start_server()                 